#include<cstdlib>

class B{
};

class A{
  private:
    B* m_b;
  public:
    A() : m_b(new B) {}
    ~A() { delete m_b; }
};

int main(void){
  A* a = new A;
  return 0;
}
